<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender')
            ->add('firstName')
            ->add('lastName')
            ->add('adress')
            ->add('country')
            ->add('nationality')
            ->add('passport')
            ->add('cv')
            ->add('profilPicture')
            ->add('currentLocation')
            ->add('dateBirth')
            ->add('placeBirth')
            ->add('emailAdress')
            ->add('emailComfirm')
            ->add('password')
            ->add('passwordComfirm')
            ->add('availability')
            ->add('jobSector')
            ->add('experience')
            ->add('shortDescription')
            ->add('note')
            ->add('dateCreated')
            ->add('dateUpdated')
            ->add('dateDeleted')
            ->add('files')
            ->add('isAdmin')
            ->add('deletedAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
