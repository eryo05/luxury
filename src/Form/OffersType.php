<?php

namespace App\Form;

use App\Entity\Offers;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OffersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference')
            ->add('companyName')
            ->add('contactName')
            ->add('contactEmail')
            ->add('number')
            ->add('description')
            ->add('enabled')
            ->add('note')
            ->add('jobTitle')
            ->add('jobType')
            ->add('location')
            ->add('jobCategory')
            ->add('closingDate')
            ->add('salary')
            ->add('creationDate')
            ->add('deletedAt')
            ->add('customersCustomer')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offers::class,
            'attr' => [
                'class' => 'form-inline']
        ]);
    }
}
