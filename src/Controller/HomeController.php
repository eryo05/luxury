<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/compagny", name="compagny", methods="GET|POST")
     */
    public function compagny()
    {
        
        return $this->render('home/compagny.html.twig');
    }

    /**
     * @Route("/contact", name="contact", methods="GET|POST")
     */
    public function contact()
    {
        
        return $this->render('home/contact.html.twig');
    }
}
