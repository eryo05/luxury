<?php

namespace App\Controller;

use App\Entity\Users;
use App\Entity\Candidates;
use App\Entity\Offers;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

    /**
     * @Route("/security")
     */

class SecurityController extends AbstractController
{
    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new Users();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user , $user->getPassword());
            $user->setPassword($hash);
            $user->setPasswordComfirm($hash);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_login');
        }  

        return $this->render("security/registration.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connexion", name="security_login" )
     */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="security_logout" )
     */
    public function logout()
    {

    }
    /**
     * @Route("/admin", name="security_admin" )
     */
    public function admin(UserInterface $users)
    {
        $userId = $users->getIdUser(); 
       

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Users::class)->find($userId);
        // dd($product);
        $candidates = $this->getDoctrine()
        ->getRepository(Candidates::class)
        ->findAll();
        //dd($candidates);

        if ($product->getIsAdmin()){

            return $this->render('security/admin.html.twig', ['candidates' => $candidates]);

        }
        else {
            return $this->render('home/index.html.twig');

        }

        
    }
}
