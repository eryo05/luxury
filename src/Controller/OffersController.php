<?php

namespace App\Controller;

use App\Entity\Offers;
use App\Entity\Candidates;
use App\Form\OffersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\NoResultException;

/**
 * @Route("/offers")
 */
class OffersController extends AbstractController
{
    /**
     * @Route("/index", name="offers_index", methods="GET")
     */
    public function index(): Response
    {
        $offers = $this->getDoctrine()
            ->getRepository(Offers::class)
            ->findAll();
            //dd($offers);

        return $this->render('offers/index.html.twig', ['offers' => $offers]);
    }

    /**
     * @Route("/new", name="offers_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $offer = new Offers();
        $form = $this->createForm(OffersType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return $this->redirectToRoute('offers_index');
        }

        return $this->render('offers/new.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idOffer}", name="offers_show", methods="GET")
     */
    public function show(Offers $offer): Response
    {
        //dd($offer->getIdOffer());
        $nextOffer = $this->getSiblingOffer($offer, 'next');
        $previousOffer = $this->getSiblingOffer($offer, 'previous');

        return $this->render('offers/show.html.twig', compact('nextOffer', 'previousOffer', 'offer'));
    }

    private function getSiblingOffer(Offers $offer, $direction)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $siblingOffer = $em->getRepository(Offers::class)->createQueryBuilder('tc');
            $siblingOffer->setMaxResults( 1 );

            if($direction == "next") {
                $siblingOfferId = $offer->getIdOffer()+1;
            }
            else {
                $siblingOfferId = $offer->getIdOffer()-1;
            }

            $siblingOffer->where('tc.idOffer = '.$siblingOfferId);
            $siblingOffer = $siblingOffer->getQuery()->getSingleResult();
        }
        catch(NoResultException $e) {
            $siblingOffer = $em->getRepository(Offers::class)->createQueryBuilder('tc');
            $siblingOffer->setMaxResults( 1 );

            if($direction == "next") {
                $siblingOffer->orderBy('tc.idOffer', "ASC");
            }
            else {
                $siblingOffer->orderBy('tc.idOffer', "DESC");
            }
            $siblingOffer = $siblingOffer->getQuery()->getSingleResult();
        }

        return $siblingOffer;
    }

    /**
     * @Route("/{idOffer}/edit", name="offers_edit", methods="GET|POST")
     */
    public function edit(Request $request, Offers $offer): Response
    {
        $form = $this->createForm(OffersType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('offers_edit', ['idOffer' => $offer->getIdOffer()]);
        }

        return $this->render('offers/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idOffer}", name="offers_delete", methods="DELETE")
     */
    public function delete(Request $request, Offers $offer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$offer->getIdOffer(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($offer);
            $em->flush();
        }

        return $this->redirectToRoute('offers_index');
    }

    /**
     * @Route("/add", name="offers_add", methods="GET|POST")
     */
    public function add(Request $request): Response
    {
        $offer = new Offers();
        $data = $request->request->all();
        
        //dd($data);
        $em = $this->getDoctrine()->getManager();
        $offer->setJobType($data['job_type']);
        $offer->setReference($data['reference']);
        $offer->setCompanyName($data['compagny_name']);
        $offer->setContactName($data['contact_name']);
        $offer->setContactEmail($data['contact_email']);
        $offer->setNumber($data['number']);
        $offer->setDescription($data['description']);
        $offer->setLocation($data['location']);
        //$offer->setClosingDate($data['closing_date']);
        $offer->setSalary($data['salary']);
        $offer->setNote($data['note']);
        $offer->setJobCategory($data['job_category']);
        $offer->setJobTitle($data['job_title']);
        $date = new \DateTime('@'.strtotime('now'));
        $offer->setCreationDate($date);
        
        //dd($offer);
        $em->persist($offer);
        $em->flush();
         
        return $this->render('security/admin.html.twig', [
            'offer' => $offer,
            
        ]);
    }

    /**
     * @Route("/{idOffer}/candidate", name="offers_candidate", methods="GET|POST")
     */
    public function candidat(Request $request, Offers $offer,UserInterface $users): Response
    {
        $nextOffer = $this->getSiblingOffer($offer, 'next');
        $previousOffer = $this->getSiblingOffer($offer, 'previous');

        $user = $this->getUser();
        $candidate = new Candidates();

        $em = $this->getDoctrine()->getManager();
        $candidate->setUsers($user);
        $candidate->setOffers($offer);
        $date = new \DateTime('@'.strtotime('now'));
        $candidate->setCreatedAt($date);
        //dd($candidate);

        $em->persist($candidate);
        $em->flush();

        // dd($candidate);
        
        
        return $this->render('offers/show.html.twig', compact('offer', 'previousOffer', 'nextOffer'));
    }
}
