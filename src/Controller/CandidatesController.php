<?php

namespace App\Controller;

use App\Entity\Candidates;
use App\Form\CandidatesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/candidates")
 */
class CandidatesController extends AbstractController
{
    /**
     * @Route("/", name="candidates_index", methods="GET")
     */
    public function index(): Response
    {
        $candidates = $this->getDoctrine()
            ->getRepository(Candidates::class)
            ->findAll();

        return $this->render('candidates/index.html.twig', ['candidates' => $candidates]);
    }

    /**
     * @Route("/new", name="candidates_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $candidate = new Candidates();
        $form = $this->createForm(CandidatesType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();

            return $this->redirectToRoute('candidates_index');
        }

        return $this->render('candidates/new.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idUser}", name="candidates_show", methods="GET")
     */
    public function show(Candidates $candidate): Response
    {
        return $this->render('candidates/show.html.twig', ['candidate' => $candidate]);
    }

    /**
     * @Route("/{idUser}/edit", name="candidates_edit", methods="GET|POST")
     */
    public function edit(Request $request, Candidates $candidate): Response
    {
        $form = $this->createForm(CandidatesType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('candidates_edit', ['idUser' => $candidate->getIdUser()]);
        }

        return $this->render('candidates/edit.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/del", name="candidates_delete", methods="POST")
     */
    public function delete(Request $request): Response
    {       
        $data = $request->request->all();
        $id_candidat = $data['candidate_id'];
        //dd($id_candidat);
        $em = $this->getDoctrine()->getManager();
        $candidate = $em->getRepository(Candidates::class)->find($id_candidat);

            
            $em->remove($candidate);
            $em->flush();
        

        return $this->redirectToRoute('security_admin');
    }
}
